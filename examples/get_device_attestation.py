#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Exemplary execution of API supported KYM device attestation flows.

KYM = Know Your Machine

Note:

For access to the KYM Attestation API, OAuth user credentials need to
be provided. Put these two lines into a YAML file at
`${HOME}/.config/pysinglesource` for things to work:

    oauth_consumer_key: "<your_user_name>"
    oauth_consumer_secret: "<your_secret_credentials>"
"""

import base64
import logging
import textwrap
import typing

from singlesource.attestations import (api_request_kym_attestation,
                                       get_raw_subject_commitment,
                                       merge_subject_commitment)
from singlesource.config import config
from singlesource.dids import (generate_id,
                               make_did_document,
                               api_register_did_document,
                               api_resolve_did_document,
                               public_authentication_jwk_from_did_doc)


# This is the DID used by Jasmy's KYM Host,
# on behalf of whom the attestation will be made.
KYM_HOST_DID = 'did:ssid:5CokW795eKdqA9aFYfB6u6chR5QfeM9dxPFi8a7Nfb4kSeLN'

# The DID of the KYM API expressing the attestation.
ATTESTER_DID = 'did:ssid:5HWydV3Lo8ofFf4xLjsxudMmF3DwrAeGSCPcb8dW19AvAU3x'

# The info about the device we want to attest.
DEVICE_CLAIMS = [
    {
        "@context": "http://schema.org",
        "@type": "Thing",
        "identifier": [
            {"@type": "PropertyValue",
             "serialNumber": "4711-0815"},
            {"@type": "PropertyValue",
             "name": "MAC address",
             "value": "9c:de:ad:be:ef:42"}
        ]
    }
]

# The public signing keys.
# TODO: (Note) This should just require the DID of the identity,
#       then retrieve this key using the DID registry (to come soon).
ATTESTER_SIG_JWK = {
    "kty": "OKP",
    "crv": "Ed25519",
    "x": "8TakswfV80LFiX_Ii1dJL9u4vqWlkhApKbo-v2i1uUc"
}
DEVICE_SIG_KEY = {
    "kty": "OKP",
    "crv": "Ed25519",
    "x": "11qYAYKxCrfVS_7TyWQHOg7hcvPapiMlrwIaaPcHURo"
}
DEVICE_SIG_PUB_KEY = base64.urlsafe_b64decode(DEVICE_SIG_KEY['x'] + '==')

# Private device signing key seed.
DEVICE_SIG_KEY_SEED = base64.urlsafe_b64decode(
    'nWGxne_9WmC6hEr0kuwsxERJxWl7MmkZcDusAxyuf2A==')


def imid_flow():
    """Execute the flow for a 'limited' device (cannot EdDSA sign)."""
    # Make a new device IMID (random generated).
    device_imid = generate_id(id_type='imid')

    # Request a KYM attestation from the API and output the results.
    api_result = api_request_kym_attestation(
        subject=device_imid, claims=DEVICE_CLAIMS,
        attester_obo=KYM_HOST_DID, statements=config.kym_default_statements)

    # Some output.
    attestation_filename = ('_attestation_{}.jwe'
                            .format(device_imid.replace(':', '_')))
    attestation_id_filename = ('_attestation_id_{}.txt'
                               .format(device_imid.replace(':', '_')))
    with open(attestation_filename, 'wt') as fd:
        fd.write(api_result['attestation'])
    with open(attestation_id_filename, 'wt') as fd:
        fd.write(api_result['attestation_id'])
    print_output = [
        'Sample attestation for a limited device with an IMID.',
        'IMID: {}'.format(device_imid),
        'Attestation ID: {}'.format(api_result['attestation_id']),
        'Attestation in file {}'.format(attestation_filename)
    ]
    _nice_print(print_output)


def did_flow():
    """Execute the flow for a 'full device' (can EdDSA sign)."""
    # This is the blockchain address of the device
    # (derived from the SR25519 public key).
    device_address_chain_address = (
        '5DEZLP9ADUPbVEYKgJ2TS1tfMaSeVQCNbpiBXX5oH4XcUDXc')

    # The DID of the device, using the blockchain address as an identifier.
    device_did = generate_id(identifier=device_address_chain_address)

    # The public (Ed25519) signing key to register.
    sig_pub_key_bytes = (b'\xd7Z\x98\x01\x82\xb1\n\xb7\xd5K\xfe\xd3\xc9d\x07:'
                         b'\x0e\xe1r\xf3\xda\xa6#%\xaf\x02\x1ah\xf7\x07Q\x1a')
    sig_pub_key = {
        'key_type': 'Ed25519VerificationKey2018',
        'public_key': base64.b64encode(sig_pub_key_bytes).decode('utf-8'),
        'authentication_type': 'Ed25519SignatureAuthentication2018'
    }

    # Make the DID document.
    device_did_document = make_did_document(device_did, [sig_pub_key])

    # Register the DID document.
    api_register_did_document(device_did, device_did_document)

    # Request a KYM attestation from the API and output the results.
    api_result = api_request_kym_attestation(
        subject=device_did, claims=DEVICE_CLAIMS,
        attester_obo=KYM_HOST_DID, statements=config.kym_default_statements)

    # Extract raw subject attestation payload (for external signing).
    attester_did_document = api_resolve_did_document(ATTESTER_DID)
    attester_sig_key = public_authentication_jwk_from_did_doc(
        attester_did_document)
    raw_subj_commitment = get_raw_subject_commitment(api_result['attestation'],
                                                     attester_sig_key)

    raw_attestation = api_result['attestation']

    # Get a (detached) Ed25519 signature from the device as a (64) bytes array.
    subj_commitment_signature = _get_faked_device_signature(
        raw_subj_commitment.encode('utf-8'))

    # Now merge the signed subject commitment into the attestation.
    device_sig_key = public_authentication_jwk_from_did_doc(
        device_did_document)
    accepted_attestation = merge_subject_commitment(
        raw_attestation, attester_sig_key, device_sig_key,
        raw_subj_commitment, subj_commitment_signature)

    # Some output.
    attestation_filename = ('_attestation_{}.jwe'
                            .format(device_did.replace(':', '_')))
    attestation_id_filename = ('_attestation_id_{}.txt'
                               .format(device_did.replace(':', '_')))
    with open(attestation_filename, 'wt') as fd:
        fd.write(accepted_attestation)
    with open(attestation_id_filename, 'wt') as fd:
        fd.write(api_result['attestation_id'])
    print_output = [
        'Sample attestation for a full device with a DID.',
        'DID: {}'.format(device_did),
        'Attestation ID: {}'.format(api_result['attestation_id']),
        'Attestation in file {}'.format(attestation_filename)
    ]
    _nice_print(print_output)


def _get_faked_device_signature(raw_subj_commitment: bytes):
    """Get a subject commitment signature."""
    import nacl.signing
    signer = nacl.signing.SigningKey(DEVICE_SIG_KEY_SEED)
    return signer.sign(raw_subj_commitment).signature


def _nice_print(content: typing.List[str]):
    """Print the provided output nicely."""
    delimiter = '-----'
    print_output = [delimiter] + content + [delimiter]
    print(textwrap.indent('\n'.join(print_output), '| '))


if __name__ == '__main__':
    # Configure logging level to get better insights.
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(levelname)s\t%(name)s\t%(asctime)s %(message)s')

    # Exercise two complete device attestation flows.
    imid_flow()
    did_flow()
