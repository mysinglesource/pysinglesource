# -*- coding: utf-8 -*-
"""Tests for the attestations module."""

# Created: 2019-04-11 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.
#
# This work is licensed under the Apache 2.0 open source licence.
# Terms and conditions apply.
#
# You should have received a copy of the licence along with this
# program.

__author__ = 'Guy K. Kloss <guy@mysinglesource.io>'

import base64
import json
from unittest import mock  # @UnusedImport
import unittest
from sspyjose.jwk import Jwk
from singlesource.attestations import (api_request_kym_attestation,
                                       make_foreign_claim_set,
                                       attest_claim_set,
                                       AttesterData,
                                       AttestationStatement,
                                       AttestationData,
                                       get_raw_subject_commitment,
                                       merge_subject_commitment)
from singlesource.config import config


DID = 'did:ssid:5FYngEEqqFKYti1PwgqzkxCb1rPuhMyDEGfVfrd73dyWewyt'
IMID = 'imid:ssid:5FYngEEqqFKYti1PwgqzkxCb1rPuhMyDEGfVfrd73dyWewyt'
KYM_CLAIMS = [
    {
        "@context": "http://schema.org",
        "@type": "Thing",
        "identifier": [
            {"@type": "PropertyValue",
             "serialNumber": "4711-0815"},
            {"@type": "PropertyValue",
             "name": "MAC address",
             "value": "9c:de:ad:be:ef:42"}
        ]
    }
]
JWK_HARRY_FULL_ED25519 = (
    '{"kty":"OKP"'
    ',"crv":"Ed25519"'
    ',"d":"nWGxne_9WmC6hEr0kuwsxERJxWl7MmkZcDusAxyuf2A"'
    ',"x":"11qYAYKxCrfVS_7TyWQHOg7hcvPapiMlrwIaaPcHURo"'
    ',"kid":"Harry James Potter (student)"}')
JWK_HARRY_PUB_ED25519 = (
    '{"kty":"OKP"'
    ',"crv":"Ed25519"'
    ',"x":"11qYAYKxCrfVS_7TyWQHOg7hcvPapiMlrwIaaPcHURo"'
    ',"kid":"Harry James Potter (student)"}')
JWK_ALBUS_PUB_ED25519 = (
    '{"kty":"OKP"'
    ',"crv":"Ed25519"'
    ',"x":"8TakswfV80LFiX_Ii1dJL9u4vqWlkhApKbo-v2i1uUc"'
    ',"kid":"Albus Percival Wulfric Brian Dumbledore (Headmaster)"}')
JWK_ALBUS_FULL_ED25519 = (
    '{"kty":"OKP"'
    ',"crv":"Ed25519"'
    ',"d":"Y-pua--r4vGiRcZieji_sdH5feNsRJgTXBEiOlEmfWc"'
    ',"x":"8TakswfV80LFiX_Ii1dJL9u4vqWlkhApKbo-v2i1uUc"'
    ',"kid":"Albus Percival Wulfric Brian Dumbledore (Headmaster)"}')

RAW_SUBJ_COMMITMENT = (
    'eyJhbGciOiJFZDI1NTE5In0.eyJjb21taXRtZW50IjoiYmxhaCBibGFoIGJsYWgiLCJpc3M'
    'iOiJkaWQ6c3NpZDptaXJyb3JvZmVyaXNlZCIsInN1YiI6ImRpZDpzc2lkOm1pcnJvcm9mZX'
    'Jpc2VkIiwicm9sZSI6InN1YmplY3QifQ')
SUBJ_COMMITMENT_SIG = (
    b'\xad\xd2\xc7\xca:\xe3\x86IvW\xaf\x8b\x0c\xbc\xef\x7f\x85\x92?\xfa\xc7'
    b'\xe0>\x8e\x10\xfez\x0cJ\xbc\x8a+\x1c\xdb\xd6;\xa6\xa0{\xa0\xfd\x07m|'
    b'\xd8\x96\x98x\xba\xd4\x94\\q\x12-\xba$\x04s\xe3U/1\x06')


class AttestationsModuleTest(unittest.TestCase):
    """Testing the attestations module functions."""

    def setUp(self):  # noqa: D102
        pass

    def tearDown(self):  # noqa: D102
        pass

    @mock.patch('singlesource.utils.request_api', autospec=True)
    def test_api_request_kym_attestation(self, request_api_mock):
        """Request a KYM attestation via the API."""
        response = mock.Mock()
        response.status_code = 201
        response.content = (b'{"attestationId": "08/15-4711",'
                            b' "attestation": "my attestation"}')
        request_api_mock.return_value = response
        result = api_request_kym_attestation(IMID, KYM_CLAIMS, DID,
                                             config.kym_default_statements)
        mock_call = request_api_mock.call_args[1]
        self.assertEqual(request_api_mock.call_count, 1)
        self.assertEqual(mock_call['method'], 'POST')
        self.assertEqual(mock_call['base_url'],
                         config.kym_attestation_base_url)
        self.assertEqual(mock_call['resource'],
                         config.kym_attestation_resource_path)
        self.assertIsInstance(json.loads(mock_call['body']), dict)
        self.assertDictEqual(result, {'attestation_id': '08/15-4711',
                                      'attestation': 'my attestation'})

    def test_attest_claim_set_device_full_flow(self):
        """Attest an unencrypted device claim set with a full flow."""
        attester_sig_key = Jwk.get_instance(from_json=JWK_ALBUS_FULL_ED25519)
        claim_set_keys = make_foreign_claim_set(KYM_CLAIMS, DID, device=True)
        claim_set = claim_set_keys.claim_set
        attester_obo = config.kym_default_attester_obo
        attester_data = AttesterData(
            iss='did:ssid:5FuuQKEtXvHXpMdQFMC4Fq4zrqjowGhQKQwygsrbv87Jr5P6',
            delegation_chain=[attester_obo])
        statements = [
            AttestationStatement(exp=item.get('exp'),
                                 ttl=item.get('ttl'),
                                 metadata=item.get('metadata'))
            for item in config.kym_default_statements]
        evidence_verification = ('kauriid:procedures/evidence_checks'
                                 '/ipfs/QmdXXXcurrentmanualXXX...YgV')
        attestation_data = AttestationData(
            provenance_namespaces={},
            evidence_elements={},
            evidence_verification=evidence_verification,
            ancestors=[],
            statements=statements)
        result = attest_claim_set(None,
                                  attester_data,
                                  attestation_data,
                                  attester_sig_key=attester_sig_key,
                                  claim_set=claim_set,
                                  device=True)
        self.assertSetEqual(set(result.keys()),
                            {'id', 'attestation', 'claim_set_keys'})
        self.assertIsNone(result['claim_set_keys'])
        self.assert_(result['id'].startswith('kauriid:attestations/'))
        self.assert_(len(result['attestation']) > 1000)

    @mock.patch('singlesource.attestations.Attestation', autospec=True)
    def test_get_raw_subject_commitment(self, Attestation_mock):  # noqa: N803
        """Extract a raw subject commitment from attestation."""
        attester_commitment = {
            'commitment': 'blah blah blah',
            'iss': 'did:ssid:dumbledore',
            'sub': 'did:ssid:mirroroferised',
            'role': 'attester'}
        subject_commitment_check = {
            'commitment': 'blah blah blah',
            'iss': 'did:ssid:mirroroferised',
            'sub': 'did:ssid:mirroroferised',
            'role': 'subject'}
        my_attestation_mock = mock.MagicMock()
        my_attestation_mock.commitment_payloads = {
            'attester': attester_commitment}
        my_attestation_mock.commitments = {
            'attester': 'eyJhbGciOiJFZDI1NTE5In0.blah.blah'}
        Attestation_mock.return_value = my_attestation_mock
        result = get_raw_subject_commitment('certificate of authenticity',
                                            'silver key')
        self.assertEqual(Attestation_mock.call_count, 1)
        self.assertEqual(my_attestation_mock.load.call_count, 1)
        result_parts = [json.loads(base64.urlsafe_b64decode(item + '=='))
                        for item in result.split('.')]
        self.assertDictEqual(result_parts[0], {'alg': 'Ed25519'})
        self.assertDictEqual(result_parts[1], subject_commitment_check)

    @mock.patch('singlesource.attestations.Jws', autospec=True)
    @mock.patch('singlesource.attestations.Attestation', autospec=True)
    def test_merge_subject_commitment(self,
                                      Attestation_mock,  # noqa: N803
                                      Jws_mock):
        """Merge a signed subject commitment into an attestation."""
        jws_mock = mock.MagicMock()
        jws_mock.verify.return_value = True
        jws_mock.payload = 'heavy stuff'
        Jws_mock.get_instance.return_value = jws_mock
        my_attestation_mock = mock.MagicMock()
        my_attestation_mock.serialise.return_value = 'ducks in a row'
        Attestation_mock.return_value = my_attestation_mock

        result = merge_subject_commitment(
            'the thing we have had before',
            'attester squiggle',
            'subject squiggle',
            RAW_SUBJ_COMMITMENT,
            SUBJ_COMMITMENT_SIG)

        self.assertEqual(result, 'ducks in a row')
        self.assertEqual(Attestation_mock.call_count, 1)
        self.assertEqual(my_attestation_mock.serialise.call_count, 1)
        self.assertEqual(Jws_mock.get_instance.call_count, 1)
        self.assertEqual(jws_mock.verify.call_count, 1)
        mock_call = Jws_mock.get_instance.call_args[1]
        self.assertEqual(len(mock_call['from_compact']),
                         len(RAW_SUBJ_COMMITMENT) + 87)


if __name__ == '__main__':
    unittest.main()
