# -*- coding: utf-8 -*-
"""Tests utilities for PySingleSource."""

# Created: 2019-03-15 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.
#
# This work is licensed under the Apache 2.0 open source licence.
# Terms and conditions apply.
#
# You should have received a copy of the licence along with this
# program.

__author__ = 'Guy K. Kloss <guy@mysinglesource.io>'

_counter = 0


def reset_random_mock_counter():
    """
    Reset the static/global `_counter` variable for `random_mocker()`.
    """
    global _counter
    _counter = 0


def random_mocker(increment: int = 1):
    """
    Generate a mocker for a random bytes generator.

    Uses a sequence of left zero padded incremented bytes, only the lowest
    byte will be incremented modulo 256 (max. sequence of 256 values).

    :param increment: Increment for the least significant byte.
    """
    def mocker(length: int):
        global _counter
        result = bytes(length - 1) + bytes([_counter])
        _counter = (_counter + increment) % 256
        return result

    return mocker
