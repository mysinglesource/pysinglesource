# -*- coding: utf-8 -*-
"""Tests for the irds module."""

# Created: 2019-03-15 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.
#
# This work is licensed under the Apache 2.0 open source licence.
# Terms and conditions apply.
#
# You should have received a copy of the licence along with this
# program.

__author__ = 'Guy K. Kloss <guy@mysinglesource.io>'

import base64
import unittest

from singlesource.idrs import EphemeralHandler


SERIALISED_HANDLER = 'XhAfHd7yp5iQafMzB3RDGRWT5VbR5JvR6YimGZ6AXQr9LagJc'
HANDLER_JWK = {
    'kty': 'OKP',
    'crv': 'X25519',
    'd': 'RbDuVxG7AIMJnII_HCG1D7imN7r95bJgUOd6Rig23Dk',
    'x': 'z4J2GT2BR447bA2QLL2gB6BGCsDj9cTwEWf-Q1j5VxQ'
}
HANDLER_PRIV = base64.urlsafe_b64decode(HANDLER_JWK['d'] + '==')
HANDLER_PUB = base64.urlsafe_b64decode(HANDLER_JWK['x'] + '==')
HANDLER_TOPIC = 'YRWLZZMHXuV31Ff9w135EA'


class EphemeralHandlerTest(unittest.TestCase):
    """Testing the EphemeralHandler module functions."""

    def setUp(self):  # noqa: D102
        pass

    def tearDown(self):  # noqa: D102
        pass

    def test_vanilla_constructor(self):
        """Construct a vanilla handler."""
        handler = EphemeralHandler()
        self.assertEqual(len(handler._ephemeral_jwk.d), 32)
        self.assertEqual(len(handler._ephemeral_jwk.x), 32)

    def test_deserialise_constructor(self):
        """Construct a handler from serialised data."""
        handler = EphemeralHandler(SERIALISED_HANDLER)
        self.assertEqual(handler._ephemeral_jwk.d, HANDLER_PRIV)
        self.assertEqual(handler._ephemeral_jwk.x, HANDLER_PUB)

    def test_deserialise(self):
        """Deserialise a handler from data."""
        handler = EphemeralHandler()
        handler._deserialise(SERIALISED_HANDLER)
        self.assertEqual(handler._ephemeral_jwk.d, HANDLER_PRIV)
        self.assertEqual(handler._ephemeral_jwk.x, HANDLER_PUB)

    def test_deserialise_fail(self):
        """Deserialise a handler with bad data."""
        handler = EphemeralHandler()
        self.assertRaises(ValueError, handler._deserialise, 'foo')

    def test_serialise(self):
        """Serialise a handler to data."""
        handler = EphemeralHandler(SERIALISED_HANDLER)
        result = handler.serialise()
        self.assertEqual(result, SERIALISED_HANDLER)

    def test_messaging_topic(self):
        """Get the messaging topic of a handler."""
        handler = EphemeralHandler(SERIALISED_HANDLER)
        result = handler.messaging_topic
        self.assertEqual(result, HANDLER_TOPIC)

    def test_public_private_key(self):
        """Get public and private key from the handler."""
        handler = EphemeralHandler(SERIALISED_HANDLER)
        self.assertEqual(handler.private_key, HANDLER_PRIV)
        self.assertEqual(handler.public_key, HANDLER_PUB)

    def test_jwk(self):
        """Get the handler's JWK object."""
        handler = EphemeralHandler(SERIALISED_HANDLER)
        result = handler.jwk
        self.assertDictEqual(result.to_dict(), HANDLER_JWK)


if __name__ == '__main__':
    # import sys; sys.argv = ['', 'Test.testName']
    unittest.main()
