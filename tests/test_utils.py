# -*- coding: utf-8 -*-
"""Tests for the attestations module."""

# Created: 2019-04-11 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.
#
# This work is licensed under the Apache 2.0 open source licence.
# Terms and conditions apply.
#
# You should have received a copy of the licence along with this
# program.

__author__ = 'Guy K. Kloss <guy@mysinglesource.io>'

import json
import unittest

from sspyjose.jwe import Jwe
from sspyjose.jwk import Jwk

from singlesource.utils import (jwk_from,
                                encrypt_to,
                                decrypt_from)


JWK_C20P_DICT = {
    'kty': 'oct', 'alg': 'C20P', 'use': 'enc',
    'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFA'
}
JWK_X25519_DICT = {
    'kty': 'OKP',
    'crv': 'X25519',
    'd': 'U7ZBkzSFDI7uUV0RfDmFaeHxUCnFaxaib9sypeLrJ-M',
    'x': 'U1GCoOv8N0lePEpZVL0eWEOxriqW_ikAnZdDq4nAn24',
    'kid': 'Harry James Potter (student)'
}
JWE = ('eyJhbGciOiJFQ0RILUVTIiwiZW5jIjoiQzIwUCIsImVwayI6eyJrdHkiOiJPS1AiLC'
       'JjcnYiOiJYMjU1MTkiLCJ4IjoiNHo2RWZOVTlid2dZZ3V0ZWMzX3RfbnNsV2h6NDdm'
       'YzFuOUF6LXVCcmtnZyJ9fQ..b1Is1ftCaPLsm_ag.fe0GnAJ7hwEVuPchAQ.4ynYwP'
       'YOeSO4n2idtPAPVw')
KYM_CLAIMS = [
    {
        "@context": "http://schema.org",
        "@type": "Thing",
        "identifier": [
            {"@type": "PropertyValue",
             "serialNumber": "4711-0815"},
            {"@type": "PropertyValue",
             "name": "MAC address",
             "value": "9c:de:ad:be:ef:42"}
        ]
    }
]


class UtilsModuleTest(unittest.TestCase):
    """Testing the utils module functions."""

    def setUp(self):  # noqa: D102
        pass

    def tearDown(self):  # noqa: D102
        pass

    def test_jwk_from(self):
        """Getting a Jwk object from data."""
        check = Jwk.get_instance(from_dict=JWK_C20P_DICT)
        tests = [Jwk.get_instance(from_dict=JWK_C20P_DICT),
                 JWK_C20P_DICT,
                 json.dumps(JWK_C20P_DICT)]
        for test in tests:
            result = jwk_from(test)
            self.assertEqual(result.alg, check.alg)
            self.assertEqual(result.k, check.k)

    def test_encrypt_to(self):
        """Encrypting a dict to a recipient."""
        data = {'answer': 42}
        result = encrypt_to(data, JWK_X25519_DICT)
        key = Jwk.get_instance(from_dict=JWK_X25519_DICT)
        decrypter = Jwe.get_instance(alg='ECDH-ES', jwk=key,
                                     from_compact=result)
        recovered = decrypter.decrypt()
        self.assertDictEqual(data, recovered)

    def test_decrypt_from(self):
        """Decrypting a dict from a sender."""
        result = decrypt_from(JWE, JWK_X25519_DICT)
        self.assertDictEqual(result, {'answer': 42})


if __name__ == '__main__':
    unittest.main()
