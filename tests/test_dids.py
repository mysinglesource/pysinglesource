# -*- coding: utf-8 -*-
"""Tests for the dids module."""

# Created: 2019-05-08 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.
#
# This work is licensed under the Apache 2.0 open source licence.
# Terms and conditions apply.
#
# You should have received a copy of the licence along with this
# program.

__author__ = 'Guy K. Kloss <guy@mysinglesource.io>'

import json
from unittest import mock  # @UnusedImport
import unittest

import base58

from singlesource.config import config
from singlesource.dids import (generate_id,
                               _get_address_from_pub_key,
                               make_did_document,
                               api_register_did_document,
                               api_resolve_did_document,
                               public_authentication_jwk_from_did_doc)


PUB_KEY = (b"\x9a\x1f\xcc\x19\x9d\x98\x90\x18l\xba8\xf1\x992\x0f\xf3'\xbb"
           b"\x82\x9d\xb9\xb1\xb8\xb9\x80\x10\x94\x00\xd8\xb7\xb5t")
IDENTIFIER = '5FYngEEqqFKYti1PwgqzkxCb1rPuhMyDEGfVfrd73dyWewyt'
DID = 'did:ssid:5FYngEEqqFKYti1PwgqzkxCb1rPuhMyDEGfVfrd73dyWewyt'
IMID = 'imid:ssid:5FYngEEqqFKYti1PwgqzkxCb1rPuhMyDEGfVfrd73dyWewyt'
DID_DOC = {
    "@context": "https://w3id.org/did/v1",
    "id": "did:ssid:5FYngEEqqFKYti1PwgqzkxCb1rPuhMyDEGfVfrd73dyWewyt",
    "authentication": [
        {"type": "Ed25519SignatureAuthentication2018",
         "publicKey": "did:ssid:5FYngEEqqFKYti1PwgqzkxCb1rPuhMyDEGfVfrd73dyWewyt#key1"}  # noqa: E501
    ],
    "publicKey": [
        {"id": "did:ssid:5FYngEEqqFKYti1PwgqzkxCb1rPuhMyDEGfVfrd73dyWewyt#key1",  # noqa: E501
         "type": "Ed25519VerificationKey2018",
         "controller": "did:ssid:5FYngEEqqFKYti1PwgqzkxCb1rPuhMyDEGfVfrd73dyWewyt",  # noqa: E501
         "publicKeyBase64": "QCFPBLm5pwmuTOu+haxv0+Vpmr6Rrz/DEEvbcjktQnQ="},
        {"id": "did:ssid:5FYngEEqqFKYti1PwgqzkxCb1rPuhMyDEGfVfrd73dyWewyt#key2",  # noqa: E501
         "type": "Curve25519EncryptionPublicKey",
         "controller": "did:ssid:5FYngEEqqFKYti1PwgqzkxCb1rPuhMyDEGfVfrd73dyWewyt",  # noqa: E501
         "publicKeyBase64": "qjqXWrLmfoP7yyzzCztXWNPJvoMiXXBJPIuo9Azquhg="}
    ]
}
PUBLIC_KEYS = [
    {'key_type': 'Ed25519VerificationKey2018',
     'public_key': 'QCFPBLm5pwmuTOu+haxv0+Vpmr6Rrz/DEEvbcjktQnQ=',
     'authentication_type': 'Ed25519SignatureAuthentication2018'},
    {'key_type': 'Curve25519EncryptionPublicKey',
     'public_key': 'qjqXWrLmfoP7yyzzCztXWNPJvoMiXXBJPIuo9Azquhg='}
]


class DidsModuleTest(unittest.TestCase):
    """Testing the dids module functions."""

    def setUp(self):  # noqa: D102
        pass

    def tearDown(self):  # noqa: D102
        pass

    def test_generate_id_did(self):
        """Generate a DID."""
        self.assertEqual(generate_id(identifier=IDENTIFIER), DID)

    def test_generate_id_imid(self):
        """Generate an IMID."""
        self.assertEqual(generate_id(identifier=IDENTIFIER, id_type='imid'),
                         IMID)

    def test_generate_id_did_from_pubkey(self):
        """Generate a DID from a public key."""
        self.assertEqual(generate_id(public_key=PUB_KEY), DID)

    def test_generate_id_imid_random(self):
        """Generate an IMID using a random identifier."""
        imid = generate_id(id_type='imid')
        self.assert_(imid.startswith('imid:ssid:'))
        self.assertEqual(len(imid), len(IMID))
        self.assertEqual(len(base58.b58decode(imid[10:])), 35)

    def test__get_address_from_pub_key(self):
        """Generate an identifier from a public key."""
        tests = [
            (PUB_KEY, 'did', IDENTIFIER),
            (b'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF',
             'did',
             '5Der5t8CfDpMXQNr5Gn84Yg6sWjRzEo82vLnaV1eh7KBZefy'),
            (b' \xd4u7\x10\x06\xf85\x0f\xa1G5\x87.\xebS\xb2\x075\x8eE\xd71'
             b'\x00\xf8\xa7\x0c\xfef\xcc\xf5q',
             'did',
             '5CokW795eKdqA9aFYfB6u6chR5QfeM9dxPFi8a7Nfb4kSeLN'),
            (b'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF',
             'imid',
             '7KBhWiVfjLBcodsb6TFczUG2Shjc9623zfyYVqWyR4oRUgrq'),
            (b' \xd4u7\x10\x06\xf85\x0f\xa1G5\x87.\xebS\xb2\x075\x8eE\xd71'
             b'\x00\xf8\xa7\x0c\xfef\xcc\xf5q',
             'imid',
             '7JLbvwWYiS16SP4zZqebq2CczGQqoCNZv8tU3vchPYYzMYho')
        ]
        for test, id_type, expected in tests:
            self.assertEqual(_get_address_from_pub_key(test, id_type=id_type),
                             expected)

    def test_make_did_document(self):
        """Make a DID document."""
        result = make_did_document(DID, PUBLIC_KEYS)
        self.assertDictEqual(json.loads(result), DID_DOC)

    @mock.patch('singlesource.utils.request_api', autospec=True)
    def test_api_register_did_document(self, request_api_mock):
        """Register a DID document with the API."""
        response = mock.Mock()
        response.status_code = 204
        request_api_mock.return_value = response
        api_register_did_document(DID, 'my did doc')
        mock_call = request_api_mock.call_args[1]
        self.assertEqual(request_api_mock.call_count, 1)
        self.assertEqual(mock_call['base_url'], config.did_registry_base_url)
        self.assertEqual(mock_call['resource'],
                         '/diddocuments/did:ssid:5FYngEEqqFKYti1PwgqzkxCb1r'
                         'PuhMyDEGfVfrd73dyWewyt')
        self.assertEqual(mock_call['body'], 'my did doc')
        self.assertEqual(mock_call['method'], 'PUT')

    @mock.patch('singlesource.utils.request_api', autospec=True)
    def test_api_resolve_did_document(self, request_api_mock):
        """Resolve a DID document with the API."""
        response = mock.Mock()
        response.status_code = 200
        response.content = b'{"@context": "foo bar", "content": "my did doc"}'
        request_api_mock.return_value = response
        result = api_resolve_did_document(DID)
        self.assertEqual(result,
                         '{"@context": "foo bar", "content": "my did doc"}')
        mock_call = request_api_mock.call_args[1]
        self.assertEqual(request_api_mock.call_count, 1)
        self.assertEqual(mock_call['base_url'], config.did_registry_base_url)
        self.assertEqual(mock_call['resource'],
                         '/diddocuments/did:ssid:5FYngEEqqFKYti1PwgqzkxCb1r'
                         'PuhMyDEGfVfrd73dyWewyt')
        self.assertNotIn('method', mock_call)
        self.assertNotIn('body', mock_call)

    def test_public_authentication_jwk_from_did_doc(self):
        """Get pub signing JWK from DID doc."""
        result = public_authentication_jwk_from_did_doc(json.dumps(DID_DOC))
        self.assertEqual(
            result.x,
            b'@!O\x04\xb9\xb9\xa7\t\xaeL\xeb\xbe\x85\xaco\xd3\xe5i\x9a\xbe'
            b'\x91\xaf?\xc3\x10K\xdbr9-Bt')
        self.assertEqual(result.crv, 'Ed25519')


if __name__ == '__main__':
    # import sys; sys.argv = ['', 'Test.testName']
    unittest.main()
