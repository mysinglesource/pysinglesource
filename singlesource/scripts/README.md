CLI to the SingleSource SDK
===========================

DIDs
----

* Making a DID from a (blockchain) address.

    singlesource/scripts/singlesource_cli.py dids --operation generate \
        --address 5FYngEEqqFKYti1PwgqzkxCb1rPuhMyDEGfVfrd73dyWewyt

* Make a DID from a public signing key.

    singlesource/scripts/singlesource_cli.py dids --operation generate \
        --pub-sig-key qjqXWrLmfoP7yyzzCztXWNPJvoMiXXBJPIuo9Azquhg=

* Generate a new IMID from a random data.

    singlesource/scripts/singlesource_cli.py dids --operation generate \
        --type imid

* Generate a DID document with a public (signing) key.

    singlesource/scripts/singlesource_cli.py dids --operation makedoc \
        --did did:ssid:5FuuQKEtXvHXpMdQFMC4Fq4zrqjowGhQKQwygsrbv87Jr5P6 \
        --pub-sig-key qjqXWrLmfoP7yyzzCztXWNPJvoMiXXBJPIuo9Azquhg= \
        --did-doc examples/_did_ssid_5FuuQKEtXvHXpMdQFMC4Fq4zrqjowGhQKQwygsrbv87Jr5P6.json

* Register a DID document with the DID registry.

    singlesource/scripts/singlesource_cli.py dids --operation register \
        --did did:ssid:5FuuQKEtXvHXpMdQFMC4Fq4zrqjowGhQKQwygsrbv87Jr5P6 \
        --did-doc examples/_did_ssid_5FuuQKEtXvHXpMdQFMC4Fq4zrqjowGhQKQwygsrbv87Jr5P6.json


Attestations
------------

* Attest a device with its data data via the KYM API.

    singlesource/scripts/singlesource_cli.py attestations --operation attest \
        --subject imid:ssid:5FuuQKEtXvHXpMdQFMC4Fq4zrqjowGhQKQwygsrbv87Jr5P6 \
        --claims examples/device_claims.json \
        --attestation examples/_imid_ssid_5FuuQKEtXvHXpMdQFMC4Fq4zrqjowGhQKQwygsrbv87Jr5P6.json

